//
//  User.m
//  ShopLikes
//
//  Created by Parag Dulam on 05/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic user_id;
@dynamic name;
@dynamic email;
@dynamic first_name;
@dynamic gender;
@dynamic last_name;



@end
