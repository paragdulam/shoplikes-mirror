//
//  Location.m
//  ShopLikes
//
//  Created by Parag Dulam on 05/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "Location.h"


@implementation Location

@dynamic location_id;
@dynamic name;

@end
