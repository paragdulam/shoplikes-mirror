//
//  Like.m
//  ShopLikes
//
//  Created by Parag Dulam on 05/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "Like.h"


@implementation Like

@dynamic like_id;
@dynamic name;
@dynamic category;
@dynamic created_time;

@end
