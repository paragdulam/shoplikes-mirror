//
//  Location.h
//  ShopLikes
//
//  Created by Parag Dulam on 05/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * location_id;
@property (nonatomic, retain) NSString * name;

@end
