//
//  Like.h
//  ShopLikes
//
//  Created by Parag Dulam on 05/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Like : NSManagedObject

@property (nonatomic, retain) NSString * like_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * created_time;

@end
