//
//  SLAppManager.m
//  ShopLikes
//
//  Created by Parag Dulam on 03/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "SLAppManager.h"
#import "FBSDKLoginKit.h"
#import "FBSDKCoreKit.h"
#import "User.h"
#import "Like.h"
#import "Product.h"
#import "AppDelegate.h"


@implementation SLAppManager


+(BOOL) isLoggedIn
{
    return [FBSDKAccessToken currentAccessToken] ? YES : NO;
}

#pragma mark - Network Requests

+(void) loginWithCompletionHandler:(void(^)(id obj,NSError *error))completionBlock
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email",@"user_likes",@"user_friends",@"user_about_me"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                [SLAppManager userInfoForLoggedInUserWithCompletionBlock:^(id obj, NSError *error) {
                }];
            }
        }
        completionBlock(result,error);
    }];
}


+(void) userInfoForLoggedInUserWithCompletionBlock:(void(^)(id obj,NSError *error))completionBlock
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"/me"] parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             //Save User Here
             [SLAppManager saveUser:result completionHandler:^(id obj, NSError *error) {
                 
             }];
             [SLAppManager likesForUserId:[result objectForKey:@"id"] completionHandler:^(id obj, NSError *error) {
                 //Save Likes Here
             }];
         }
     }];
}


+(void) likesForUserId:(NSString *) userId completionHandler:(void(^)(id obj,NSError *error))completionBlock
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"%@/likes",userId] parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             for (NSDictionary *likeDict in [result objectForKey:@"data"]) {
                 [SLAppManager saveLike:likeDict completionHandler:^(id obj, NSError *error) {
                     completionBlock(obj,error);
                 }];
             }
         }
     }];
}


+(void) favouritesForUserId:(NSString *) userId completionHandler:(void(^)(id obj,NSError *error))completionBlock
{
    
}


#pragma mark - CoreData Caching Methods

+(AppDelegate *) appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+(NSManagedObjectContext *) context
{
    return [[SLAppManager appDelegate] managedObjectContext];
}

+(void) saveLike:(NSDictionary *) like completionHandler:(void(^)(id obj,NSError *error))completionBlock
{
    Like *toBeSavedLike = [self likeWithLikeId:[like objectForKey:@"id"]];
    if (!toBeSavedLike) {
        toBeSavedLike = (Like *)[NSEntityDescription insertNewObjectForEntityForName:@"Like" inManagedObjectContext:[SLAppManager context]];
    }
    toBeSavedLike.like_id = [like objectForKey:@"id"];
    toBeSavedLike.name = [like objectForKey:@"name"];
    toBeSavedLike.category = [like objectForKey:@"category"];
    toBeSavedLike.created_time = [like objectForKey:@"created_time"];
    
    NSError *error = nil;
    [[SLAppManager context] save:&error];
    completionBlock(toBeSavedLike,error);
}

+(void) saveProduct:(NSDictionary *) product completionHandler:(void(^)(id obj,NSError *error))completionBlock
{
    
}


+(Like *) likeWithLikeId:(NSString *) likeId
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Like"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"like_id == %@",likeId];
    [fetchRequest setPredicate:predicate];
    return [[[SLAppManager context] executeFetchRequest:fetchRequest error:nil] firstObject];
}

+(User *) userWithUserId:(NSString *) userId
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@",userId];
    [fetchRequest setPredicate:predicate];
    return [[[SLAppManager context] executeFetchRequest:fetchRequest error:nil] firstObject];
}

+(void) saveUser:(NSDictionary *) user completionHandler:(void(^)(id obj,NSError *error))completionBlock
{
    User *toBeSavedUser = [SLAppManager userWithUserId:[user objectForKey:@"id"]];
    if (!toBeSavedUser) {
        toBeSavedUser = (User *)[NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:[SLAppManager context]];
    }
    toBeSavedUser.user_id = [user objectForKey:@"id"];
    toBeSavedUser.name = [user objectForKey:@"name"];
    toBeSavedUser.email = [user objectForKey:@"email"];
    toBeSavedUser.first_name = [user objectForKey:@"first_name"];
    toBeSavedUser.gender = [user objectForKey:@"gender"];
    toBeSavedUser.last_name = [user objectForKey:@"last_name"];
    [[SLAppManager context] save:nil];
}





@end
