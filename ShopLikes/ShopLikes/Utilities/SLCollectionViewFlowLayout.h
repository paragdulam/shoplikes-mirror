//
//  SLCollectionViewFlowLayout.h
//  ShopLikes
//
//  Created by Parag Dulam on 14/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLCollectionViewFlowLayout : UICollectionViewFlowLayout

@end
