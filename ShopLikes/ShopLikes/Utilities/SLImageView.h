//
//  SLImageView.h
//  ShopLikes
//
//  Created by Parag Dulam on 05/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLImageView : UIImageView

-(void) setImageURL:(NSURL *)imageURL forImageId:(NSString *) imageId;

@end
