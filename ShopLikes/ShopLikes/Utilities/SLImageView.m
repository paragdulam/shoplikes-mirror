//
//  SLImageView.m
//  ShopLikes
//
//  Created by Parag Dulam on 05/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "SLImageView.h"

@interface SLImageView ()
{
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation SLImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self addSubview:activityIndicator];
    }
    return self;
}


-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    activityIndicator.center = CGPointMake(frame.size.width/2, frame.size.height/2);
}

-(NSString *)documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths firstObject];
}

-(void) setImageURL:(NSURL *)imageURL forImageId:(NSString *) imageId
{
    NSString *imagePath = [NSString stringWithFormat:@"%@/%@",[self documentsDirectory],imageId];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        __block NSData *imageData = nil;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            imageData = [NSData dataWithContentsOfFile:imagePath];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setImage:[UIImage imageWithData:imageData]];
            });
        });
    } else {
        [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:imageURL] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [data writeToFile:[NSString stringWithFormat:@"%@/%@",[self documentsDirectory],imageId]
                       atomically:YES];
            });
            [self setImage:[UIImage imageWithData:data]];
        }];
    }
}


@end
