//
//  SLAppManager.h
//  ShopLikes
//
//  Created by Parag Dulam on 03/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface SLAppManager : NSObject

+(void) loginWithCompletionHandler:(void(^)(id obj,NSError *error))completionBlock;
+(void) userInfoForLoggedInUserWithCompletionBlock:(void(^)(id obj,NSError *error))completionBlock;
+(void) likesForUserId:(NSString *) userId completionHandler:(void(^)(id obj,NSError *error))completionBlock;
+(NSManagedObjectContext *) context;
+(BOOL) isLoggedIn;



@end
