//
//  SLCarouselCollectionReusableView.m
//  ShopLikes
//
//  Created by Parag Dulam on 09/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "SLCarouselCollectionReusableView.h"
#import "SLAppManager.h"
#import "SLLikeCell.h"
#import <CoreData/CoreData.h>

@interface SLCarouselCollectionReusableView ()<NSFetchedResultsControllerDelegate>
@property (nonatomic,strong) NSFetchedResultsController *likesFetchedResultsController;
@end


@implementation SLCarouselCollectionReusableView

#pragma mark - View Life Cycle

-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.carousel = [[iCarousel alloc] initWithFrame:self.bounds];
        self.carousel.type = iCarouselTypeCoverFlow;
        self.carousel.dataSource = self;
        self.carousel.delegate = self;
        [self addSubview:self.carousel];
        
        NSError *error;
        if (![[self likesFetchedResultsController] performFetch:&error]) {
            // Update to handle the error appropriately.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
    return self;
}


#pragma mark - Helper Methods

-(void) reloadData
{
    [self.carousel reloadData];
}

#pragma mark - Setters and Getters

- (NSFetchedResultsController *)likesFetchedResultsController {
    
    if (_likesFetchedResultsController != nil) {
        return _likesFetchedResultsController;
    }
    
    NSManagedObjectContext *managedObjectContext = [SLAppManager context];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Like"
                                   inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"created_time" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                  managedObjectContext:managedObjectContext
                                                                                                    sectionNameKeyPath:nil
                                                                                                             cacheName:@"Root"];
    self.likesFetchedResultsController = theFetchedResultsController;
    _likesFetchedResultsController.delegate = self;
    
    return _likesFetchedResultsController;
}

#pragma mark - iCarouselDataSource,iCarouselDelegate


- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [[self.likesFetchedResultsController fetchedObjects] count];
}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    SLLikeCell *cell = (SLLikeCell *)view;
    if (!cell) {
        cell = [[SLLikeCell alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    }
    [cell setLike:[[self.likesFetchedResultsController fetchedObjects] objectAtIndex:index]];
    cell.backgroundColor = [UIColor redColor];
    return cell;
}


#pragma mark - NSFetchedResultsControllerDelegate

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    
}



@end
