//
//  SLLikeCell.h
//  ShopLikes
//
//  Created by Parag Dulam on 09/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Like.h"

@interface SLLikeCell : UIView

-(void) setLike:(Like *) alike;

@end
