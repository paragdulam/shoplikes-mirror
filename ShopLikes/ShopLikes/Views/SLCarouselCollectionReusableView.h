//
//  SLCarouselCollectionReusableView.h
//  ShopLikes
//
//  Created by Parag Dulam on 09/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface SLCarouselCollectionReusableView : UICollectionReusableView<iCarouselDataSource,iCarouselDelegate>

@property (nonatomic,strong) iCarousel *carousel;
-(void) reloadData;

@end
