//
//  SLLikeCell.m
//  ShopLikes
//
//  Created by Parag Dulam on 09/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "SLLikeCell.h"
#import "SLImageView.h"


@interface SLLikeCell ()
@property (nonatomic) SLImageView *imageView;
@end

@implementation SLLikeCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.imageView = [[SLImageView alloc] initWithFrame:self.bounds];
        [self addSubview:self.imageView];
    }
    return self;
}



-(void) setLike:(Like *) alike
{
    NSString *imageURLString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=square",alike.like_id];
    NSURL *imageURL = [NSURL URLWithString:imageURLString];
    [self.imageView setImageURL:imageURL forImageId:alike.like_id];
}

@end
