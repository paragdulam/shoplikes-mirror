//
//  SLBaseCollectionViewController.h
//  ShopLikes
//
//  Created by Parag Dulam on 03/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "SLBaseViewController.h"

@interface SLBaseCollectionViewController : SLBaseViewController
{
    UICollectionView *baseCollectionView;
}


@end
