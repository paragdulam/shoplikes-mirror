//
//  SLBaseCollectionViewController.m
//  ShopLikes
//
//  Created by Parag Dulam on 03/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "SLBaseCollectionViewController.h"
#import "SLCarouselCollectionReusableView.h"
#import "SLCollectionViewFlowLayout.h"

@interface SLBaseCollectionViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>

@end

@implementation SLBaseCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    SLCollectionViewFlowLayout *flowLayout = [[SLCollectionViewFlowLayout alloc] init];
    baseCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                            collectionViewLayout:flowLayout];
    flowLayout.headerReferenceSize = CGSizeMake(baseCollectionView.frame.size.width, 200);
    baseCollectionView.alwaysBounceVertical = YES;
    [baseCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
    [baseCollectionView registerClass:[SLCarouselCollectionReusableView class]
           forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                  withReuseIdentifier:@"sectionheader"];
    baseCollectionView.dataSource = self;
    baseCollectionView.delegate = self;
    [self.view addSubview:baseCollectionView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UICollectionViewCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor redColor];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        SLCarouselCollectionReusableView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                                    withReuseIdentifier:@"sectionheader"
                                                                                           forIndexPath:indexPath];
        cell.frame = CGRectMake(0, 0, collectionView.frame.size.width, 200);
        cell.backgroundColor = [UIColor greenColor];
        [cell reloadData];
        return cell;
    }
    return nil;
}


@end
