//
//  SLLikesViewController.m
//  ShopLikes
//
//  Created by Parag Dulam on 03/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "SLLikesViewController.h"
#import "SLAppManager.h"
#import <CoreData/CoreData.h>
#import "FBSDKCoreKit.h"
#import "AppDelegate.h"

@interface SLLikesViewController ()<NSFetchedResultsControllerDelegate>

@property (nonatomic,strong) NSFetchedResultsController *productsFetchedResultsController;

@end

@implementation SLLikesViewController
@synthesize productsFetchedResultsController = _productsFetchedResultsController;


#pragma mark - Setters and Getters



#pragma mark - View Controller Life Cycle


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.frame = CGRectMake(0, 0, 35, 35);
    [loginButton setBackgroundColor:[UIColor redColor]];
    [loginButton addTarget:self action:@selector(loginButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loginButton];
    if ([SLAppManager isLoggedIn]) {
        [SLAppManager likesForUserId:@"me" completionHandler:^(id obj, NSError *error) {
            [baseCollectionView reloadData];
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBActions


-(void)loginButtonTapped:(UIButton *) btn
{
    [SLAppManager loginWithCompletionHandler:^(id obj, NSError *error) {
    }];
}


#pragma mark - NSFetchedResultsControllerDelegate



@end
