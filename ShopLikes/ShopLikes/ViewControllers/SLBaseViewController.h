//
//  SLBaseViewController.h
//  ShopLikes
//
//  Created by Parag Dulam on 03/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLAppConstants.h"

@class AppDelegate;

@interface SLBaseViewController : UIViewController

@property (weak,nonatomic) AppDelegate *appDelegate;

@end
