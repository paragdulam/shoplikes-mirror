//
//  SLAppConstants.h
//  ShopLikes
//
//  Created by Parag Dulam on 03/04/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#ifndef ShopLikes_SLAppConstants_h
#define ShopLikes_SLAppConstants_h

#define SLNAVIGATIONBAR_COLOR [UIColor colorWithRed:87.f/255.f green:117.f/255.f blue:216.f/255.f alpha:1.f]

#endif
